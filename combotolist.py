
from PyQt6.uic import loadUi
from PyQt6.QtWidgets import QApplication

class ComboToList:
    def __init__(self):
        self.mainui = loadUi('combotolist.ui')
        self.mainui.show()
        self.mainui.pbAddCmb.clicked.connect(self.addlist)
        self.mainui.pbDeleteItems.clicked.connect(self.deleteitem)
        
    def addlist(self):
        cmbdata = self.mainui.cmbOpt.currentText()
        if cmbdata == 'Please Select Items':
            self.mainui.lblMsg.setText('Please Select Data From Combo Box')
        else:
            self.mainui.listMain.addItem(cmbdata)
            
    def deleteitem(self):
        selected_items = self.mainui.listMain.selectedItems()
        
        if selected_items:
            for item in selected_items:
                row = self.mainui.listMain.row(item)
                self.mainui.listMain.takeItem(row)
        else:
            self.mainui.lblMsg.setText("Please Select Data From List Box To Delete")

if __name__ == '__main__':
    app = QApplication([])
    main = ComboToList()
    app.exec()